* Getting in Tune.
One of creative coding works by deconbatch.
[[https://deconbatch.blogspot.com/2018/02/getting-in-tune.html]['Getting in Tune.' on Creative Coding : Examples with Code.]]
** Description
Magnified version of [[https://deconbatch.blogspot.com/2017/12/interstellar-overdrive.html][Interstellar Overdrive]].
I love that shape and I wanted to see the detail.

This code do not display any images on screen but generates image files for animation. 
** Change log
   - updated : 2019.05.26
     - stop rotate, fixed in horizontal position
     - change frame rate from 20fps to 24fps
   - updated : 2018.04.30
     - refactored
   - created : 2018.02.24
