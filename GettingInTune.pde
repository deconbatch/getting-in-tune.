/**
 * Getting in Tune.
 * Draw some animation with my custom noise formula.
 * 
 * @author @deconbatch
 * @version 0.3
 * Processing 3.2.1
 * created 2018.02.24
 * updated 2018.04.30 refactored
 * updated 2019.05.26 tune movement
 */

float halfW;
float halfH;
float baseRadius;
float baseHue;
float seedSize;
float seedShape;
float pitchTune;

void setup() {
  
  size(720, 720);
  colorMode(HSB, 360.0, 100.0, 100.0, 100.0);
  smooth();

  halfW      = width / 2;
  halfH      = height / 2;
  baseRadius = halfH;
  baseHue    = random(360);
  seedSize   = random(100);

  getOutTune();  // start with out of tune

}

void draw() {

  background(0.0, 0.0, 0.0, 100.0);
  translate(halfW, halfH);

  blendMode(SCREEN);
  drawShape();
  if (frameCount % 140 == 0) {
    getOutTune();
  }

  blendMode(BLEND);
  drawScope();

  // for 24fps x 16s animation
  saveFrame("frames/####.png");
  if (frameCount > 24 * 16) {
    exit ();
  }

}

float customNoise(float value) {
  return pow(sin(value), 3) * cos(pow(value, 2));
}

void getOutTune() {
  // define new random shape & get out the tuning
  seedShape = radians(180) * (ceil(random(34)) * 3);  // magic number for nice shapes
  pitchTune = 1.0;
  baseHue  += 60;
}

void drawShape() {

  noStroke();
  float noiseSize  = seedSize;
  float noiseShape = seedShape;
  float noiseTune  = pitchTune;
  
  // this weird 'for' sentence is for centering the shape
  for (float ptX = -(halfW + 150); ptX <= halfW; ptX += 0.8) {

    float ptY   = map(customNoise(noiseTune) + customNoise(noiseShape), -2.0, 2.0, -baseRadius, baseRadius);
    float ptSiz = map(abs(customNoise(noiseSize)), 0.0, 1.0, 10.0, 40.0);
    float ptSat = map(abs(customNoise(noiseSize)), 0.0, 1.0, 0.0, 40.0);
    float ptBri = map(abs(customNoise(noiseSize)), 0.0, 1.0, 20.0, 10.0);
    float ptAlp = 100.0;
    float ptHue = (
                  baseHue
                  + map(ptX, -halfW, halfW, 0, 30)
                  + map(ptY, -baseRadius, baseRadius, 0, 90)
                  )
                  % 360;

    // draw glow point
    for (float i = 1.0; i < ptSiz; ++i) {
      fill(
           ptHue,
           map(i, 1.0, ptSiz, ptSat, 80.0),
           map(i, 1.0, ptSiz, ptBri, 5.0),
           ptAlp
           );
      ellipse(ptX, ptY, i, i);
    }

    noiseSize  += 0.003;
    noiseShape += 0.0024;	// IMPORTANT! shape size == scope size
    noiseTune  += pitchTune;

  }

  seedSize  += 0.003;	            // blink
  seedShape += 0.002 / seedShape; // roll & scroll
  pitchTune /= 1.3;	              // geting in tune

}

void drawScope() {
  strokeWeight(200.0);
  stroke(0.0, 0.0, 100.0, 100.0);
  fill(0.0, 0.0, 0.0, 0.0);
  ellipse(0.0, 0.0, width + 200.0, height + 200.0);
}
